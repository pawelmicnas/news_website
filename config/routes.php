<?php

require('services.php');

return [
    'GET' => [
        '' => [
            'controller' => 'PageController@index',
        ],
        'registration' => [
            'controller' => 'PageController@registration',
        ],
        'users' => [
            'controller' => 'UserController@showAll',
            'services' => [
                'database' => $database,
            ],
        ],
        'login' => [
            'controller' => 'UserController@login',
            'services' => [
                'database' => $database,
            ],
        ],
        'logout' => [
            'controller' => 'UserController@logout',
            'services' => [
                'database' => $database,
            ],
        ],
        'news' => [
            'controller' => 'NewsController@showAll',
            'services' => [
                'database' => $database,
            ],
        ],
        'news/add' => [
            'controller' => 'NewsController@addNews',
            'services' => [
                'database' => $database,
            ],
        ],
        'news/edit' => [
            'controller' => 'NewsController@editNews',
            'services' => [
                'database' => $database,
            ],
        ],
        'csv/import' => [
            'controller' => 'CsvController@csv',
            'services' => [
                'database' => $database,
            ],
        ],
        'csv/chart' => [
            'controller' => 'CsvController@chart',
            'services' => [
                'database' => $database,
            ],
        ],
    ],
    'POST' => [
        'users/save' => [
            'controller' => 'UserController@saveUser',
            'services' => [
                'database' => $database,
            ],
        ],
        'login/check' => [
            'controller' => 'UserController@checkLogin',
            'services' => [
                'database' => $database,
            ],
        ],
        'news/save' => [
            'controller' => 'NewsController@saveNews',
            'services' => [
                'database' => $database,
            ],
        ],
        'news/update' => [
            'controller' => 'NewsController@updateNews',
            'services' => [
                'database' => $database,
            ],
        ],
        'csv/post-file' => [
            'controller' => 'CsvController@postCsvFile',
            'services' => [
                'database' => $database,
            ],
        ],
    ],
];