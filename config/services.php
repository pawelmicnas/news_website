<?php

$parameters = require_once('parameters.php');

$database = [
    'class' => 'Framework\\Database\\Database',
    'parameters' => [
        $parameters['dbHost'],
        $parameters['dbName'],
        $parameters['dbUser'],
        $parameters['dbPassword'],
    ],
];