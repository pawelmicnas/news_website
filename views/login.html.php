<?php require 'base/header.html.php'; ?>

<form class="form" action="/login/check" method="POST">
    <div class="row text-center form-group">
        <div class="col-md-12">
            <label for="email">email</label>
            <input class="form-control" id="email" type="text" name="email">
            <label for="password">password</label>
            <input class="form-control" id="password" type="password" name="password">
        </div>
    </div>
    <div class="row text-center">
        <div class="col-md-12">
            <button type="submit">Zaloguj się</button>
        </div>
    </div>
</form>

<?php require 'base/footer.html.php'; ?>
