<?php require 'base/header.html.php'; ?>

<div class="row text-center">
    <h1>Wykres</h1>
    <canvas id="myChart" width="400" height="400"></canvas>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script>
    const ctx = document.getElementById("myChart").getContext('2d');
    const myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["<?= implode('", "', array_keys($data)) ?>"],
            datasets: [{
                label: '# of Votes',
                data: [<?= implode(', ', array_values($data)) ?>],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>

<?php require 'base/footer.html.php'; ?>
