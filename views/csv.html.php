<?php require 'base/header.html.php'; ?>

<div class="row text-center">
    <h1>Csv import</h1>

    <?php if (isset($_SESSION['csv']['message'])) : ?>

        <div class="col-md-5 text center">
            <div class="alert alert-danger"><?= $_SESSION['csv']['message'] ?></div>
        </div>

    <?php endif ?>

    <div class="col-md-12">
        <form class="form" action="/csv/post-file" method="post" enctype="multipart/form-data">
            Select csv to upload:
            <input type="file" class="form-control" name="file" id="file">
            <input type="submit" class="form-control" value="Upload csv" name="submit">
        </form>
    </div>
</div>

<?php require 'base/footer.html.php'; ?>
