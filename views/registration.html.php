<?php require 'base/header.html.php'; ?>

<div class="row">
    <div class="col-md-12">
        <h1>Rejestracja</h1>
    </div>
</div>


<form class="form" action="users/save" method="POST">

    <div class="row">
        <div class="col-md-12">
            <label for="first_name">Imię</label>
            <input id="first_name" class="form-control" type="text" name="first_name">
        </div>
        <div class="col-md-12">
            <label for="last_name">Nazwisko</label>
            <input id="last_name" class="form-control" type="text" name="last_name">
        </div>
        <div class="col-md-12">
            <label for="gender">Płeć</label>
            <select id="gender" name="gender" class="form-control">
                <option value="male">Mężczyzna</option>
                <option value="female">Kobieta</option>
            </select>
        </div>
        <div class="col-md-12">
            <label for="email">Email</label>
            <input id="email" type="text" name="email" class="form-control">
        </div>
        <div class="col-md-12">
            <label for="password">Hasło</label>
            <input id="password" class="form-control" type="password" name="password">
        </div>
    </div>

    <div class="row text-center">
        <div class="col-md-12">
            <button type="submit">Zarejestruj</button>
        </div>
    </div>
</form>


<?php require 'base/footer.html.php'; ?>
