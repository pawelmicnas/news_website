<?php require 'base/header.html.php'; ?>

<div class="row">
    <div class="col-md-12">
        <h1>Dodaj news</h1>
    </div>
</div>


<form class="form" action="/news/save" method="POST">
    <div class="row text-center form-group">
        <div class="col-md-12">
            <label for="name">Tytuł</label>
            <input class="form-control" id="name" type="text" name="name">
        </div>
        <div class="col-md-12 form-group">
            <label for="description">Opis</label>
            <textarea class="form-control" id="description" name="description"></textarea>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-md-12">
            <button type="submit">Zapisz</button>
        </div>
    </div>
</form>

<?php require 'base/footer.html.php'; ?>
