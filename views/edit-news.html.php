<?php require 'base/header.html.php'; ?>

<div class="row">
    <div class="col-md-12">
        <h1>Edytuj news</h1>
    </div>
</div>

<form class="form" action="/news/update" method="POST">
    <div class="row text-center form-group">
        <div class="col-md-12">
            <p style="display:none">
                <input name="id" value="<?= $news->getId() ?>">
            </p>
            <label for="name">Tytuł</label>
            <input class="form-control" id="name" type="text" name="name"
                                                  value="<?= $news->getName() ?>"
            >
        </div>
        <div class="col-md-12 form-group">
            <label for="description">Opis</label>
            <textarea class="form-control" id="description" name="description">
                <?= $news->getDescription() ?>
            </textarea>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-md-12">
            <button type="submit">Zapisz</button>
        </div>
    </div>
</form>

<?php require 'base/footer.html.php'; ?>
