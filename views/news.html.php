<?php require 'base/header.html.php'; ?>

<h1 class="text-center">All news</h1>

<?php foreach ($news as $n) { ?>

    <div class="row">
        <div class=col-md-12><h3><?= $n['name'] ?></h3></div>
        <div class=col-md-12><p>Data utworzenia: <?= $n['created_at'] ?>;
                autor: <?= $n['first_name'] . ' ' . $n['last_name'] ?></p></div>
        <hr/>
        <div class=col-md-12 text-justify><p><?= $n['description'] ?></p></div>
        <?php if (isset($_SESSION['authenticated_user'])) : ?>
            <div class=col-md-12 text-justify>
                <p><a href="/news/edit?id=<?= $n['id'] ?>">Edytuj</a></p>
            </div>
        <?php endif; ?>
    </div>

<?php } ?>

<a class="btn btn-primary" href="/news/add">Dodaj nowy news</a>


<?php require 'base/footer.html.php'; ?>
