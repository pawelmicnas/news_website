<?php require 'base/header.html.php'; ?>

<div class="row">
    <h1>All users</h1>

    <?php foreach ($users as $user) { ?>
        <div class=col-md-12><?= $user->getFirstName() . ' ' . $user->getLastName() ?></div>
    <?php } ?>
</div>

<?php require 'base/footer.html.php'; ?>
