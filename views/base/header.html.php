<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
  <div class="container">
  <div class="text-right">
    <?php if (isset($_SESSION['authenticated_user'])) : ?>
      <a href="/logout">Wyloguj</a>
    <?php else : ?>
      <a href="/login">Zaloguj</a>
    <?php endif ?>
  </div>
