<?php require 'base/header.html.php'; ?>

<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Zadanie rekrutacyjne - spis treści</h1>
    </div>
    <div class="col-md-12">
        <ol>
            <li><a href="/news">Serwis newsów</a></li>
            <li><a href="/registration">Rejestracja</a></li>
            <li><a href="/login">Login</a></li>
            <li><a href="/news/add">Dodawanie newsów</a></li>
            <li><a href="/csv/import">Import csv</a></li>
            <li><a href="/csv/chart">Wykres na podstawie zaimportowanych csv</a></li>
        </ol>
    </div>
</div>

<?php require 'base/footer.html.php'; ?>
