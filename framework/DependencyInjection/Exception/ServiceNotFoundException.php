<?php

declare(strict_types=1);

namespace Framework\DependencyInjection\Exception;

use Exception;
use Psr\Container\NotFoundExceptionInterface;

class ServiceNotFoundException extends Exception implements NotFoundExceptionInterface
{
}