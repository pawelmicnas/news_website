<?php

declare(strict_types=1);

namespace Framework\DependencyInjection\Factory;

use Framework\DependencyInjection\Container;

class ContainerFactory
{
    public function createContainer(?array $services): Container
    {
        $container = new Container();

        if ($services !== null) {
            foreach ($services as $alias => $service) {
                if (isset($service['parameters'])) {
                    $container->set($alias, new $service['class'](...$service['parameters']));
                    continue;
                }

                $container->set($alias, new $service['class']);
            }
        }

        return $container;
    }
}
