<?php

declare(strict_types=1);

namespace Framework\Controller;

use Framework\DependencyInjection\Container;

abstract class AbstractController
{
    /** @var Container */
    protected $app;

    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    protected function render(string $template, array $variables = []): void
    {
        extract($variables, EXTR_OVERWRITE);

        require(__DIR__ . '/../../views/' . $template);
    }

    protected function redirect(string $url): void
    {
        header('Location: ' . $url);
        exit;
    }
}
