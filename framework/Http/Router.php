<?php

declare(strict_types=1);

namespace Framework\Http;

use Framework\DependencyInjection\Container;
use Framework\DependencyInjection\Factory\ContainerFactory;
use Exception;

class Router
{
    /** @var array $routes */
    private $routes = [
        'GET' => [],
        'POST' => [],
    ];

    /** @var ContainerFactory */
    private $containerFactory;

    public function __construct(array $routes, ContainerFactory $containerFactory)
    {
        $this->routes = $routes;
        $this->containerFactory = $containerFactory;
    }

    public function direct(string $uri, string $method)
    {
        if (isset($this->routes[$method][$uri])) {
            @$app = $this->containerFactory->createContainer($this->routes[$method][$uri]['services']);
            return $this->callMethodOfController($app, ...explode('@', $this->routes[$method][$uri]['controller']));
        }

        throw new Exception('Route not found');
    }

    private function callMethodOfController(Container $app, $controller, $method)
    {
        $controller = 'App\\Controller\\' . $controller;

        return (new $controller($app))->$method();
    }
}