<?php

declare(strict_types=1);

namespace Framework\Database;

use Framework\Entity\EntityInterface;
use PDO;

class QueryBuilder
{
    /** @var PDO */
    protected $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function findOneBy(EntityInterface $entity, string $column, string $data)
    {
        $query = "SELECT * FROM {$entity->getTableName()} WHERE {$column} = '{$data}'";
        $statement = $this->connection->prepare($query);

        $statement->execute();
        return $statement->fetchObject($entity->getEntityName());
    }

    public function selectAll(EntityInterface $entity): array
    {
        $statement = $this->connection->prepare("SELECT * FROM {$entity->getTableName()}");
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS, $entity->getEntityName());
    }

    public function customQuery(string $query): array
    {
        $statement = $this->connection->prepare($query);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function customQueryFetch($query)
    {
        $statement = $this->connection->prepare($query);
        $statement->execute();

        return $statement->fetch();
    }

    public function insert(string $table, array $data): void
    {
        $columns = "`" . implode("`,`", array_keys($data)) . "`";
        $data = '\'' . implode("','", $data) . '\'';
        $query = "INSERT INTO $table ($columns) VALUES ($data)";
        $statement = $this->connection->prepare($query);

        $statement->execute();
    }

    public function update(string $table, string $where, array $data): void
    {
        $query = [];
        foreach ($data as $key => $val) {
            $query[] = "`$key`='$val'";
        }

        $sql = "UPDATE `$table` SET " . implode(', ', $query) . " WHERE $where";
        $statement = $this->connection->prepare($sql);

        $statement->execute();
    }
}
