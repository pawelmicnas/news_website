<?php

declare(strict_types=1);

namespace Framework\Entity;

use ReflectionClass;
use ReflectionException;

abstract class AbstractEntity implements EntityInterface
{
    public function getEntityName(): string
    {
        return static::class;
    }

    /**
     * @throws ReflectionException
     */
    public function getTableName(): string
    {
        return strtolower((new ReflectionClass($this->getEntityName()))->getShortName());
    }
}
