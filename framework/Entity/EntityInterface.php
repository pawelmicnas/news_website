<?php

declare(strict_types=1);

namespace Framework\Entity;

interface EntityInterface
{
    public function getEntityName(): string;
    public function getTableName(): string;
}
