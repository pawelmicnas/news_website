<?php

ini_set( 'display_errors', 'On' );
error_reporting( E_ALL );

require 'vendor/autoload.php';

session_start();

use Framework\DependencyInjection\Factory\ContainerFactory;
use Framework\Http\{Router,Request};

$router = new Router(require 'config/routes.php', new ContainerFactory());
$request = new Request();

$router->direct($request->getUri(), $request->getMethod());