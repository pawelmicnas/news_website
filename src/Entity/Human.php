<?php

declare(strict_types=1);

namespace App\Entity;

class Human
{
  protected $id;
  protected $first_name;
  protected $last_name;
  protected $email;
  protected $gender;
  protected $country;

  public function getFirstName()
  {
      return $this->first_name;
  }

  public function getLastName()
  {
      return $this->last_name;
  }

  public function getEmail()
  {
      return $this->email;
  }

  public function getGender()
  {
      return $this->gender;
  }

  public function getCountry()
  {
      return $this->country;
  }
}
