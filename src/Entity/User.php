<?php

declare(strict_types=1);

namespace App\Entity;

use Framework\Entity\AbstractEntity;

class User extends AbstractEntity
{
    private const MALE = 0;
    private const FEMALE = 1;

    /** @var int */
    protected $id;

    /** @var string */
    protected $first_name;

    /** @var string */
    protected $last_name;

    /** @var string */
    protected $email;

    /** @var string */
    protected $password;

    /** @var int */
    protected $gender;

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getGender(): int
    {
        return (int)$this->gender;
    }
}
