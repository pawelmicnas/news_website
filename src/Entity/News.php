<?php

declare(strict_types=1);

namespace App\Entity;

use Framework\Entity\AbstractEntity;

class News extends AbstractEntity
{
    protected $id;
    protected $name;
    protected $description;
    protected $is_active;
    protected $created_at;
    protected $updated_at;
    protected $author_id;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getIsActive()
    {
        return $this->is_active;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdateddAt()
    {
        return $this->updated_at;
    }

    public function getAuthorId()
    {
        return $this->author_id;
    }
}
