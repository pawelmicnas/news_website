<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\FileAlreadyExistsException;
use App\Exception\UnsupportedFileExtension;
use Framework\Database\Database;
use League\Csv\Reader;
use PDO;
use RuntimeException;

class CsvService
{
    private const UPLOAD_DIR = 'var/uploads/';

    /** @var Database */
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function uploadFile(): string
    {
        $targetFile = self::UPLOAD_DIR . basename($_FILES["file"]["name"]);
        $fileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));

        if (file_exists($targetFile)) {
            throw new FileAlreadyExistsException();
        }

        if ($fileType !== 'csv') {
            throw new UnsupportedFileExtension();
        }

        if (!move_uploaded_file($_FILES["file"]["tmp_name"], $targetFile)) {
            throw new RuntimeException();
        }

        return $targetFile;


    }

    public function insertCsv(string $targetFile): void
    {
        $csv= Reader::createFromPath($targetFile)->setHeaderOffset(0);

        $query = "
          INSERT INTO human (external_id, first_name, last_name, email, gender, country)
          VALUES (:external_id, :first_name, :last_name, :email, :gender, :country)
        ";

        $connection = $this->database->getConnection();
        $statement = $connection->prepare($query);

        foreach ($csv as $record) {
            $statement->bindValue(':external_id', $record['id'], PDO::PARAM_STR);
            $statement->bindValue(':first_name', $record['first_name'], PDO::PARAM_STR);
            $statement->bindValue(':last_name', $record['last_name'], PDO::PARAM_STR);
            $statement->bindValue(':email', $record['email'], PDO::PARAM_STR);
            $statement->bindValue(':gender', $record['gender'], PDO::PARAM_STR);
            $statement->bindValue(':country', $record['country'], PDO::PARAM_STR);

            $statement->execute();
        }
    }
}