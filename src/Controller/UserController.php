<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Framework\Controller\AbstractController;
use Framework\Database\Database;
use Framework\Database\QueryBuilder;
use Framework\DependencyInjection\Container;

class UserController extends AbstractController
{
    /** @var UserRepository */
    private $userRepository;

    /** @var Database */
    private $database;

    public function __construct(Container $app)
    {
        parent::__construct($app);
        $this->database = $this->app->get('database');
        $this->userRepository = new UserRepository($this->database->getConnection());
    }

    public function showAll(): void
    {
        if (!isset($_SESSION['authenticated_user'])) {
            $this->redirect('/login');
        }

        $this->render('users.html.php', ['users' => $this->userRepository->selectAll(new User())]);
    }

    public function saveUser(): void
    {
        $queryBuilder = new QueryBuilder($this->database->getConnection());
        $queryBuilder->insert('user', [
            'first_name' => trim($_POST['first_name']),
            'last_name' => trim($_POST['last_name']),
            'email' => trim($_POST['email']),
            'password' => hash('sha256', trim($_POST['password'])),
            'gender' => $_POST['gender'],
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
            'is_active' => 1
        ]);

        $this->redirect('/users');
    }

    public function login(): void
    {
        $this->render('login.html.php');
    }

    public function checkLogin(): void
    {
        $user = $this->userRepository->findOneBy(new User(), 'email', $_POST['email']);

        if (!hash_equals($user->getPassword(), hash('sha256', trim($_POST['password'])))) {
            $this->redirect('/login');
        }

        $_SESSION['authenticated_user'] = [
            'nick' => $user->getFirstName() . ' ' . $user->getLastName(),
            'id' => $user->getId(),
        ];

        $this->redirect('/news/add');
    }

    public function logout(): void
    {
        session_destroy();

        $this->redirect('/');
    }
}
