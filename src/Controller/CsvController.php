<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\FileAlreadyExistsException;
use App\Exception\UnsupportedFileExtension;
use App\Repository\HumanRepository;
use App\Service\CsvService;
use Framework\Controller\AbstractController;
use Framework\DependencyInjection\Container;
use Throwable;

class CsvController extends AbstractController
{
    /** @var CsvService */
    private $csvService;

    /** @var HumanRepository */
    private $humanRepository;

    public function __construct(Container $app)
    {
        parent::__construct($app);
        $this->csvService = new CsvService($this->app->get('database'));
        $this->humanRepository = new HumanRepository($this->app->get('database')->getConnection());
    }

    public function csv(): void
    {
        $this->render('csv.html.php');
    }

    public function postCsvFile(): void
    {
        if (isset($_POST['submit'])) {
            try {
                $targetFile = $this->csvService->uploadFile();
            } catch (FileAlreadyExistsException $exception) {
                $_SESSION['csv']['message'] = 'Sorry, file already exists.';
                $this->redirect('/csv/import');
            } catch (UnsupportedFileExtension $exception) {
                $_SESSION['csv']['message'] = 'File has no csv extension.';
                $this->redirect('/csv/import');
            } catch (Throwable $exception) {
                $_SESSION['csv']['message'] = 'Sorry, there was an error uploading your file.';
                $this->redirect('/csv/import');
            }

            $this->csvService->insertCsv($targetFile);
        }

        $this->redirect('/csv/chart');
    }

    public function chart(): void
    {
        $countries = $this->humanRepository->findCountries();

        $data = [];
        foreach ($countries as $country) {
            $countryName = $country['country'];
            $countPeople = $this->humanRepository->countPeopleInCountry($countryName);
            $data[$countryName] = $countPeople['sum'];
        }

        $this->render('chart.html.php', ['data' => $data]);
    }

}
