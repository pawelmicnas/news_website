<?php

declare(strict_types=1);

namespace App\Controller;

use Framework\Controller\AbstractController;

class PageController extends AbstractController
{

    public function index(): void
    {
        $this->render('index.html.php');
    }

    public function registration(): void
    {
        $this->render('registration.html.php');
    }
}
