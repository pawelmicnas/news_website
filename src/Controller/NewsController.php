<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\News;
use App\Repository\NewsRepository;
use Framework\Controller\AbstractController;
use Framework\Database\Database;
use Framework\Database\QueryBuilder;
use Framework\DependencyInjection\Container;

class NewsController extends AbstractController
{
    /** @var NewsRepository */
    private $newsRepository;

    /** @var Database */
    private $database;

    public function __construct(Container $app)
    {
        parent::__construct($app);
        $this->database = $this->app->get('database');
        $this->newsRepository = new NewsRepository($this->database->getConnection());
    }

    public function showAll(): void
    {
        $this->render('news.html.php', [
            'news' => $this->newsRepository->getAllNews()
        ]);
    }

    public function addNews(): void
    {
        if (!isset($_SESSION['authenticated_user'])) {
            $this->redirect('/login');
        }

        $this->render('add-news.html.php');
    }

    public function saveNews(): void
    {
        $queryBuilder = new QueryBuilder($this->database->getConnection());
        $queryBuilder->insert('news', [
            'name' => $_POST['name'],
            'description' => $_POST['description'],
            'is_active' => 1,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
            'author_id' => $_SESSION['authenticated_user']['id']
        ]);

        $this->redirect('/news');
    }

    public function editNews(): void
    {
        if (!isset($_SESSION['authenticated_user'])) {
            $this->redirect('/login');
        }

        $news = $this->newsRepository->findOneBy(new News(), 'id', $_GET['id']);
        if ($news === false) {
            $this->redirect('/news');
        }

        $this->render('edit-news.html.php', ['news' => $news]);
    }

    public function updateNews(): void
    {
        if (!isset($_SESSION['authenticated_user'])) {
            $this->redirect('/login');
        }
        
        $queryBuilder = new QueryBuilder($this->database->getConnection());
        $queryBuilder->update('news', 'id = ' . $_POST['id'], [
            'name' => $_POST['name'],
            'description' => $_POST['description'],
            'updated_at' => date('Y-m-d h:i:s'),
            'author_id' => $_SESSION['authenticated_user']['id']
        ]);

        $this->redirect('/news');
    }

    public function deleteNews(): void
    {
        //TODO: implement deleteNews method
    }
}
