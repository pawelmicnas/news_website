<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;

class FileAlreadyExistsException extends Exception
{
    protected $message = 'File already exists';
}