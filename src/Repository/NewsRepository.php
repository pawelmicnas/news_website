<?php

declare(strict_types=1);

namespace App\Repository;

use Framework\Database\QueryBuilder;

class NewsRepository extends QueryBuilder
{
    public function getAllNews(): array
    {
        $query = "SELECT n.*, u.first_name, u.last_name FROM news n LEFT JOIN user u ON n.author_id = u.id";

        return $this->customQuery($query);
    }
}