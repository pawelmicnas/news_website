<?php

declare(strict_types=1);

namespace App\Repository;

use Framework\Database\QueryBuilder;

class HumanRepository extends QueryBuilder
{
    public function findCountries(): array
    {
        return $this->customQuery("SELECT country FROM human GROUP BY country");
    }

    public function countPeopleInCountry(string $countryName): array
    {
        return $this->customQueryFetch("SELECT count(id) as sum FROM human WHERE country = '{$countryName}'");
    }
}